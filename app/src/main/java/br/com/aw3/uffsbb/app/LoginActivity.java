package br.com.aw3.uffsbb.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import br.com.aw3.uffsbb.library.Request;


public class LoginActivity extends ActionBarActivity {

    private EditText username = null;
    private EditText password = null;
    private Button login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        username = (EditText) findViewById(R.id.text_username);
        password = (EditText) findViewById(R.id.text_password);
        login = (Button) findViewById(R.id.button_login);
    }

    public void login(View view) {

        if (username.getText().toString().equals("anderson") && password.getText().toString().equals("admin")) {
            Toast.makeText(getApplicationContext(), "Redirecionando...",
                    Toast.LENGTH_SHORT).show();

            Intent i = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(i);

        } else {
            Toast.makeText(getApplicationContext(), "Usuário e/ou senha inválidos.",
                    Toast.LENGTH_SHORT).show();
        }
    }
}
