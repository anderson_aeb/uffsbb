package br.com.aw3.uffsbb.library;

/**
 * Created by Anderson on 15/07/2014.
 */
public class Request {

    public static final String BASE = "http://andy.tritoq.com/api/";

    public static String urlGetReservas() {
        return Request.BASE + "reserva/1";
    }

    public static String urlGetLogin() {
        return Request.BASE + "login";
    }

}
