package br.com.aw3.uffsbb.app.tab;

import br.com.aw3.uffsbb.app.MainActivity;
import br.com.aw3.uffsbb.app.R;
import br.com.aw3.uffsbb.library.JSONParser;
import br.com.aw3.uffsbb.library.Request;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.view.View.OnClickListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class ListaFragment extends Fragment implements OnClickListener {

    ListView list;
    TextView ver;
    TextView name;
    TextView api;
    Button Btngetdata;

    ArrayList<HashMap<String, String>> oslist = new ArrayList<HashMap<String, String>>();

    //URL to get JSON Array
    private static String url;

    //JSON Node Names
    private static final String TAG_OS = "reservas";
    private static final String TAG_LIVRO = "livro";
    private static final String TAG_DT_RESERVA = "datareserva";
    private static final String TAG_DT_ENTREGA = "dataentrega";

    JSONArray android = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_reserva_lista, container, false);

        url = Request.urlGetReservas();

        oslist = new ArrayList<HashMap<String, String>>();
        new JSONParse().execute();

        Btngetdata = (Button) rootView.findViewById(R.id.getdata);
        Btngetdata.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onClick(View v) {
        new JSONParse().execute();
    }

    private class JSONParse extends AsyncTask<String, String, JSONObject> {
        private ProgressDialog pDialog = new ProgressDialog(getActivity());

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            ver = (TextView) getActivity().findViewById(R.id.livro);
            name = (TextView) getActivity().findViewById(R.id.data_reserva);
            api = (TextView) getActivity().findViewById(R.id.data_entrega);
            pDialog.setMessage("carregando reservas...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONParser jParser = new JSONParser();
            // Getting JSON from URL
            JSONObject json = jParser.getJSONFromUrl(url);
            return json;
        }

        @Override
        protected void onPostExecute(JSONObject json) {

            pDialog.dismiss();
            oslist.clear();

            try {
                // Getting JSON Array from URL
                android = json.getJSONArray(TAG_OS);
                for (int i = 0; i < android.length(); i++) {
                    JSONObject c = android.getJSONObject(i);
                    // Storing  JSON item in a Variable
                    String ver = c.getString(TAG_LIVRO);
                    String name = c.getString(TAG_DT_RESERVA);
                    String api = c.getString(TAG_DT_ENTREGA);
                    // Adding value HashMap key => value
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put(TAG_LIVRO, ver);
                    map.put(TAG_DT_RESERVA, name);
                    map.put(TAG_DT_ENTREGA, api);
                    oslist.add(map);

                    list = (ListView) getActivity().findViewById(R.id.list);
                    ListAdapter adapter = new SimpleAdapter(getActivity(), oslist,
                            R.layout.list_reservas,
                            new String[]{TAG_LIVRO, TAG_DT_RESERVA, TAG_DT_ENTREGA}, new int[]{
                            R.id.livro, R.id.data_reserva, R.id.data_entrega}
                    );
                    list.setAdapter(adapter);

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}